public class ConversToHour{
	private int secend;
	public ConversToHour(int secend){
	this.secend = secend;
	}
	
	public int reduceTo24(){
		if(secend>86399){
		return secend/86399;
		}
		return secend;
	}
	
	public void hourFormat(){
		int secend = reduceTo24();
		int toHour = secend/3600;
		int toMinets = (secend%3600)/60;
		int toSecend = (secend%3600)%60;
		System.out.println("Godzina "+Integer.toString(toHour)+":"+Integer.toString(toMinets)+":"+Integer.toString(toSecend));
	}
}