
public class BooleanTableGenerator{
	private int n;
	private int m;
	private int propabilyty;
	private boolean[][] mineField = new boolean[n][m];
	
	public BooleanTableGenerator(int n, int m, int propabilyty){
		this.n = n;
		this.m = m;
		this.propabilyty = propabilyty;
		fileMineField(n,m,propabilyty);
	}
	
	public void fileMineField(int n, int m, int propabylity){
		 
		for(byte indexn = 0; indexn<n; indexn++){
			for(byte indexm = 0; indexm<m; indexm++){
				mineField[indexn][indexm] = new RandomBoolianGenerator().generator(propabylity);
			}
		}
	}
	public void printGenerator(){
		for(byte indexn = 0; indexn<n; indexn++){
			for(byte indexm = 0; indexm<m; indexm++){
				System.out.println(mineField[indexn][indexm]);
			}
		}
	}
	
	public boolean[][] getTable(){
		return mineField;
	}
}
